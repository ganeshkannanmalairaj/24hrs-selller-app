import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {IonContent,Platform } from '@ionic/angular'
import { HttpService } from '../shared/http.service';

@Component({
  selector: 'app-myproducts',
  templateUrl: './myproducts.page.html',
  styleUrls: ['./myproducts.page.scss'],
})
export class MyproductsPage implements OnInit {

@ViewChild (IonContent,{static:true})
content:IonContent
  constructor( private http: HttpService, private router: Router,private route: ActivatedRoute) {
    route.params.subscribe(val => {
      this.ProductList()
      
    });
   }
  
  ngOnInit() { 
  }
  
  productlist:any = [];

  exit(){
    this.router.navigate(['/tabs'])
  }
  ProductList(){
    this.http.post('/read_product', '').subscribe((response: any) => {
      this.productlist = response.records;
      console.log(this.productlist);
      
    }, (error: any) => {
      console.log(error);
    }
    );
  }

}
