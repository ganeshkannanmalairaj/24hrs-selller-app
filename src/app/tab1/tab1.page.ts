import { Component } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpService } from '../shared/http.service';
import Swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  constructor(private http: HttpService, private router: Router, private menu: MenuController,route: ActivatedRoute,public navCtrl: NavController) { 
    route.params.subscribe(val => {
      this.list()
    });
  }

  ngOnInit() {
    
  }

  listOfCat: any = [];
  listOfProduct: any = [];
  encodeText:any;

  offerObj = {
    card : "false",
    form : "true"
  }

  userdetails: any = JSON.parse(atob(localStorage.getItem("24hrs-user-data")));

  

  myproducts() { 
    this.router.navigate(['/myproducts'])
    // this.http.post('/read_product', '').subscribe((response: any) => {
    //   this.listOfProduct = response.records;
    //   console.log(this.listOfProduct);
    //   const encodeText:any = btoa(JSON.stringify(this.listOfProduct))
    //   console.log(encodeText)
    //   this.router.navigate(['/myproducts'], { queryParams: { order: (this.encodeText) } })
    // }, (error: any) => {
    //   console.log(error);
    // }
    // );
  }

  offer() {
    console.log(this.offerObj);
    
    this.router.navigate(['/tabs/tab4'],{ queryParams: { order: this.offerObj }})
  }

  list() {
    this.http.get('/read_category',).subscribe((response: any) => {
      this.listOfCat = response.records;
    }, (error: any) => {
      console.log(error);
    }
    );
  }

  offers(){
    this.router.navigate(['/tabs/tab4'])
  }

  views(){
    this.router.navigate(['/tabs/tab2'])
  }
}
