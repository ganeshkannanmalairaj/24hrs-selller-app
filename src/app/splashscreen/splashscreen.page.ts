import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-splashscreen',
  templateUrl: './splashscreen.page.html',
  styleUrls: ['./splashscreen.page.scss'],
})
export class SplashscreenPage implements OnInit {

  constructor( private router: Router, route: ActivatedRoute) { 
    route.params.subscribe(val => {
      setTimeout(()=>{
        this.router.navigate(['/signinpage'])
      },2000)
    });
  }

  ngOnInit() {
  }

}
